﻿//Milestone 2, CST-227, 8/4/2019, Coded completely by William Thornton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_227_Milestone_2
{
    public class MinesweeperGame : GameBoard, IPlayable
    {
        public MinesweeperGame(int s) : base(s)
        {

        }



        public void playGame()
        {
            this.Activate();
            this.FindNeighbors();
            revealGrid();
        }

        public new void revealGrid()
        {
            //while no live tile has been visited
            bool stop = false;
            int counter = 0;
            while (stop == false)
            {
                //checks if the player has won, compares the total size to the live count + the visited count, only
                // equal if the player has visited all the non-live tiles
                if ((size * size) == (counter + c))
                {
                    break;
                }
                //clears the console for the next grid
                Console.Clear();
                for (int i = 0; i < board.GetLength(0); i++)
                {
                    Console.Write("| ");
                    for (int j = 0; j < board.GetLength(1); j++)
                    {
                        
                        bool isLive = board[i, j].getLive();
                        bool isVisited = board[i, j].getVisited();
                        if (isVisited)
                        {
                            if (isLive)
                            {
                                // if a cell is live and visited, it clears the console and reveals the base class grid, and terminates the loop
                                Console.Clear();
                                base.revealGrid();
                                stop = true;
                            }
                            else
                            {
                                counter++;
                                if (board[i, j].getNeighbors() > 0)
                                {
                                    Console.Write(board[i, j].getNeighbors());
                                }
                                else
                                {
                                    Console.Write("~");
                                }
                            }
                        }
                        else
                        {
                            Console.Write("?");
                        }
                        Console.Write(" | ");
                    }
                    Console.WriteLine("");
                    Console.WriteLine("");
                }

                bool valid = false;
                while (valid != true)
                {
                    Console.Write("Enter row: ");
                    int row = Convert.ToInt32(Console.ReadLine());
                    Console.Write("Enter column: ");
                    int column = Convert.ToInt32(Console.ReadLine());
                    if (board[row, column].getVisited() != true)
                    {
                        board[row, column].setVisited(true);
                        valid = true;
                    }
                    else
                    {
                        Console.WriteLine("Pick a new cell, that one has already been visited.");
                    }
                }
            }
            Console.WriteLine("Congratulations, you've won! The game will close in 10 seconds");
            System.Threading.Thread.Sleep(10000);
            Environment.Exit(0);
        }
    }
}
