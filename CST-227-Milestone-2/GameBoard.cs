﻿//Milestone 2, CST-227, 8/4/2019, Coded completely by William Thornton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_227_Milestone_2
{
    //William Thornton, Milestone 1, 07/23/2019, I'm unable to use private classes
    //because they are in a namespace. Any advice on this would be appreciated.
    public abstract class GameBoard
    {
        public int size = 0;
        public Cell[,] board;
        Random r = new Random();
        public int c = 0;
        public GameBoard(int s)
        {
            size = s;
            board = new Cell[size, size];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    board[i, j] = new Cell(i, j);
                }
            }
        }


        public void Activate()
        {
            Cell[] liveCells = new Cell[100];
            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    if (r.Next(1, 101) < 20)
                    {
                        board[i, j].setLive(true);
                        board[i, j].setNeighbors(9);
                        liveCells[c] = board[i, j];
                        c += 1;
                    }
                }
            }
        }

        public void FindNeighbors()
        {

            int rowLimit = board.GetLength(0);
            int columnLimit = board.GetLength(1);

            for (int i = 0; i < board.GetLength(0); i++)
            {
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    int neighbors = 0;
                    for (int x = Math.Max(0, i - 1); x <= Math.Min(i + 1, rowLimit - 1); x++)
                    {
                        for (int y = Math.Max(0, j - 1); y <= Math.Min(j + 1, columnLimit - 1); y++)
                        {
                            if (x != i || y != j)
                            {
                                bool test = board[x, y].getLive();
                                if (test == true)
                                {
                                    neighbors++;
                                }
                            }
                        }
                    }
                    board[i, j].setNeighbors(neighbors);
                }
            }
        }


        public void revealGrid()
        {
            for (int i = 0; i < board.GetLength(0); i++)
            {
                Console.Write("| ");
                for (int j = 0; j < board.GetLength(1); j++)
                {
                    bool isLive = board[i, j].getLive();
                    if (isLive)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        if (board[i, j].getNeighbors() > 0)
                        {
                            Console.Write(board[i, j].getNeighbors());
                        }
                        else
                        {
                            Console.Write("~");
                        }
                    }
                    Console.Write(" | ");
                }
                Console.WriteLine("");
                Console.WriteLine("");
            }
            Console.WriteLine("Game Over! You visited a mine! The game will close in 5 seconds.");
            System.Threading.Thread.Sleep(5000);
            Environment.Exit(0);
        }
    }
}
