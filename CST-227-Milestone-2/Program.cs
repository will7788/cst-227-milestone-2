﻿//Milestone 2, CST-227, 8/4/2019, Coded completely by William Thornton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_227_Milestone_2
{
    class Program
    {
        static void Main(string[] args)
        {
            //starts the game using the created game object
            //this is the driver class
            MinesweeperGame game = new MinesweeperGame(10);
            game.playGame();
        }
    }
}
