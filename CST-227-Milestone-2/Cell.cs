﻿//Milestone 2, CST-227, 8/4/2019, Coded completely by William Thornton

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CST_227_Milestone_2
{
    //William Thornton, Milestone 1, 07/23/2019, I'm unable to use private classes
    //because they are in a namespace. Any advice on this would be appreciated.
    public class Cell
    {
        int row = -1;
        int column = -1;
        bool visited = false;
        bool live = false;
        int neighbors = 0;
        public Cell(int r, int c)
        {
            row = r;
            column = c;
        }

        //get functions
        public bool getVisited()
        {
            return visited;
        }

        public bool getLive()
        {
            return live;
        }
        public int getNeighbors()
        {
            return neighbors;
        }

        public int getRow()
        {
            return row;
        }

        public int getColumn()
        {
            return column;
        }

        //set functions

        public void setVisited(bool v)
        {
            visited = v;
        }

        public void setLive(bool l)
        {
            live = l;
        }
        public void setNeighbors(int n)
        {
            neighbors = n;
        }

        public void setRow(int r)
        {
            row = r;
        }

        public void setColumn(int c)
        {
            column = c;
        }

    }
}
